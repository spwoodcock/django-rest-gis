#!/bin/bash
set -euo pipefail

source .env
GIT_COMMIT=$(git rev-parse --short HEAD)
export DOCKER_BUILDKIT=1

docker build . \
      -t "${REG_URL}/django-rest-gis:${TAG}-master" \
      --build-arg http_proxy=$PROXY_URL \
      --build-arg https_proxy=$PROXY_URL \
      --build-arg REG_URL=$REG_URL \
      --build-arg PYTHON_VERSION=$PYTHON_VERSION \
      --build-arg NODE_VERSION=$NODE_VERSION \
      --build-arg MAINTAINER=$MAINTAINER \
      --build-arg GIT_COMMIT=$GIT_COMMIT
