import React, { Component } from 'react'
import Table from 'react-bootstrap/Table'
import Container from 'react-bootstrap/Container'
import Button from 'react-bootstrap/Button'
const axios = require('axios');


export default class Home extends Component {
    state = {
        isLoading: true,
        aoi: [],
        error: null
    }

    constructor(props) {
        super(props);
        this.handleDelete = this.handleDelete.bind(this);
    }

    fetchEmp() {
        fetch(window.API_URL + `/api/aoi/`)
            .then(response => response.json())
            .then(data =>
               this.setState({
                   aoi: data,
                   isLoading: false,
               })
            )
           .catch(error => this.setState({ error, isLoading: false }));
    }

    componentDidMount() {
        this.fetchEmp();
    }

    handleDelete(id) {
        axios.delete(window.API_URL + `/api/aoi/${id}/delete`)
    }

    render() {
        return (
            <Container style={{ marginTop: '100px' }}>
            <Button variant="secondary" style={{ float: 'right', margin: '20px' }} onClick={() => this.props.history.push('/aoi/create/')}>Add an AOI</Button>
            <Table striped bordered hover>
              <thead>
                <tr>
                  <th>ID</th>
                  <th>Area</th>
                  <th>Name</th>
                  <th>Date</th>
                  <th>Properties</th>
                  <th>Action</th>
                </tr>
              </thead>
               <tbody>
                  {!this.state.isLoading?this.state.aoi.map((item)=>{
                  return (
                     <tr key={item.id}>
                       <td>{item.id}</td>
                       <td>{item.area}</td>
                       <td>{item.name}</td>
                       <td>{item.date}</td>
                       <td>{item.properties.toString()}</td>
                       <td>{item.action}</td>
                       <td><Button onClick={() => this.props.history.push(`/aoi/${item.id}/update/`)}>Update</Button>
                       <Button variant="danger" onClick={()=>this.handleDelete(item.id)}>Delete</Button></td>
                     </tr>
                  )
                  })
                  :
                     "LOADING"
                  }
               </tbody>
            </Table>
         </Container>
        )
      }

}
