from django.contrib import admin
from django.contrib.gis.admin import OSMGeoAdmin

from aoi.models import AOI


@admin.register(AOI)
class AOIAdmin(OSMGeoAdmin):
    pass
