from django.apps import AppConfig


class AOIConfig(AppConfig):
    """Config to set variables throughout app."""

    default_auto_field = "django.db.models.BigAutoField"
    name = "aoi"
