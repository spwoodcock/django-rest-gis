from django.contrib.gis.db import models
from django.core import validators


class AOI(models.Model):
    """Model containing AOI details."""

    area = models.PolygonField(srid=4326)
    name = models.CharField(
        max_length=256,
        unique=True,
        validators=(
            validators.RegexValidator(
                regex=r"^[a-z0-9_]*$", message='Allowed characters: "a-z", "0-9", "_"'
            ),
        ),
    )
    date = models.DateTimeField()
    properties = models.JSONField(default=dict, blank=True, null=True)

    class Meta:
        ordering = ["name"]

    def __str__(self):
        return str(self.name)
