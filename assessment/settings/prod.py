import logging
import os
import sys

from .base import *

SECRET_KEY = os.getenv("SECRET_KEY")

DEBUG = False

ALLOWED_HOSTS = [os.getenv("HOST_URL")]
USE_X_FORWARDED_HOST = True

REST_FRAMEWORK = {
    "DEFAULT_RENDERER_CLASSES": ("rest_framework.renderers.JSONRenderer",)
}

# Database
# https://docs.djangoproject.com/en/3.2/ref/settings/#databases

DATABASES = {
    "default": {
        "ENGINE": "django.contrib.gis.db.backends.postgis",
        "NAME": os.getenv("POSTGRES_DB"),
        "USER": os.getenv("POSTGRES_USER"),
        "PASSWORD": os.getenv("POSTGRES_PASSWORD"),
        "HOST": "db_server",
        "PORT": "5432",
    }
}

# Django Logging Config
log_dir = os.getenv("DEFAULT_LOG_DIR", "/var/log/django")
LOGGING = {
    "version": 1,
    "disable_existing_loggers": False,
    "formatters": {
        "default": {
            "format": (
                "[%(asctime)s.%(msecs)03d]  %(levelname)s  |  %(name)s  |  %(message)s"
            ),
            "datefmt": "%Y-%m-%d %H:%M:%S",
        },
    },
    "handlers": {
        "assessmentlogfile": {
            "level": "DEBUG",
            "class": "logging.handlers.RotatingFileHandler",
            "filename": log_dir + "/assessment/assessment.log",
            "maxBytes": 1024 * 1024 * 15,  # 15MB
            "backupCount": 10,
            "formatter": "default",
        },
        "aoilogfile": {
            "level": "DEBUG",
            "class": "logging.handlers.RotatingFileHandler",
            "filename": log_dir + "/aoi/aoi.log",
            "maxBytes": 1024 * 1024 * 15,  # 15MB
            "backupCount": 10,
            "formatter": "default",
        },
        "console": {
            "level": "DEBUG",
            "class": "logging.StreamHandler",
            "formatter": "default",
        },
    },
    "loggers": {
        "": {
            "handlers": ["assessmentlogfile", "console"],
            "level": os.getenv("ROOT_LOG_LEVEL", "ERROR"),
        },
        "aoi": {
            "handlers": ["aoilogfile"],
            "level": os.getenv("DJANGO_LOG_LEVEL", "INFO"),
        },
    },
}
# Catch and log unhandled exceptions
sys.excepthook = lambda exc_type, exc_value, exc_traceback: logging.getLogger(
    "*excepthook*"
).critical("Uncaught Exception!", exc_info=(exc_type, exc_value, exc_traceback))
