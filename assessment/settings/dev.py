from .base import *

SECRET_KEY = "django-insecure-39)#r97e$i8y+)1d1v(0w^n+&)h0%_sn%d0m(b&-i81&*%#-4t"

DEBUG = True

ALLOWED_HOSTS = ["*"]

# Database
# https://docs.djangoproject.com/en/3.2/ref/settings/#databases

DATABASES = {
    "default": {
        "ENGINE": "django.contrib.gis.db.backends.postgis",
        "NAME": "db_aoi",
        "USER": "dbuser",
        "PASSWORD": "pgpass1234",
        "HOST": "db_server",
        "PORT": "5432",
    }
}
