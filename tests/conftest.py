import pytest
from rest_framework.test import APIClient


@pytest.fixture
def api_client():
    """Helper function to return DRF APIClient."""
    return APIClient
