import random
import string

import pytest
from django.contrib.gis.geos import Polygon
from model_bakery import baker


@pytest.fixture
def uabb():
    """
    Wrapper func for unfilled_aoi_bakery_batch.
    """

    def unfilled_aoi_bakery_batch(n):
        """
        Create a batch of mocked data for AOI model.
        Optional fields are left empty.

        Parameters
        ----------
        n : int
            Number of records to create.

        Returns
        -------
        uabb : Django AOI model object queryset.
        """
        uabb = baker.make(
            "aoi.AOI",
            area=Polygon(((-10, 15), (10, 15), (10, -15), (-10, -15), (-10, 15))).wkt,
            name=("".join(random.choices(string.ascii_lowercase, k=10))),
            _fill_optional=[
                "properties",
            ],
            _quantity=n,
        )
        return uabb

    return unfilled_aoi_bakery_batch


@pytest.fixture
def fabb():
    """
    Wrapper func for filled_aoi_bakery_batch.
    """

    def filled_aoi_bakery_batch(n):
        """
        Create a batch of mocked data for AOI model.
        Optional fields are filled.

        Parameters
        ----------
        n : int
            Number of records to create.

        Returns
        -------
        fabb : Django AOI model object queryset.
        """
        fabb = baker.make(
            "aoi.AOI",
            area=Polygon(((-10, 15), (10, 15), (10, -15), (-10, -15), (-10, 15))).wkt,
            name=("".join(random.choices(string.ascii_lowercase, k=10))),
            _quantity=n,
        )
        return fabb

    return filled_aoi_bakery_batch


@pytest.fixture
def fub():
    """
    Wrapper func for filled_aoi_bakery.
    """

    def filled_aoi_bakery():
        """
        Create a single mocked object of AOI model.
        Optional fields are filled

        Returns
        -------
        fub : Django AOI model object.
        """
        fub = baker.make(
            "aoi.AOI",
            area=Polygon(((-10, 15), (10, 15), (10, -15), (-10, -15), (-10, 15))).wkt,
            name="example_name",
            aoi=baker.make("aoi.AOI"),
        )
        return fub

    return filled_aoi_bakery
