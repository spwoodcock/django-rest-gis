import json

import pytest
from model_bakery import baker

from aoi.models import AOI

pytestmark = [pytest.mark.e2e, pytest.mark.django_db]


class TestAOIEndpointsAuto:
    """E2E tests using auto configured model_bakery."""

    # TODO not working with geometries, or lowercase names

    endpoint = "/api/aoi/"

    def test_list(self, api_client):
        baker.make(AOI, _quantity=3)

        response = api_client().get(self.endpoint)

        assert response.status_code == 200
        assert len(json.loads(response.content)) == 3

    def test_create(self, api_client):
        aoi = baker.prepare(AOI)
        print(aoi.name)
        aoi_json = {
            "area": aoi.area.wkt,
            "name": aoi.name,
            "date": aoi.date,
            "properties": aoi.properties,
        }

        response = api_client().post(self.endpoint, data=aoi_json, format="json")
        print(response.data)

        assert response.status_code == 201
        assert json.loads(response.content) == aoi_json

    def test_retrieve(self, api_client):
        aoi = baker.make(AOI)
        expected_json = {
            "id": aoi.id,
            "area": aoi.area.wkt,
            "name": aoi.name,
            "date": aoi.date,
            "properties": aoi.properties,
        }
        url = f"{self.endpoint}{aoi.id}/"

        response = api_client().get(url)

        assert response.status_code == 200
        assert json.loads(response.content) == expected_json

    def test_update(self, rf, api_client):
        old_aoi = baker.make(AOI)
        new_aoi = baker.prepare(AOI)
        new_aoi_dict = {
            "area": new_aoi.area.wkt,
            "name": new_aoi.name,
            "date": new_aoi.date,
            "properties": new_aoi.properties,
        }

        url = f"{self.endpoint}{old_aoi.id}/"

        response = api_client().put(url, new_aoi_dict, format="json")

        assert response.status_code == 200
        assert json.loads(response.content) == new_aoi_dict

    @pytest.mark.parametrize(
        "field",
        [
            ("area"),
            ("name"),
            ("date"),
            ("properties"),
        ],
    )
    def test_partial_update(self, mocker, rf, field, api_client):
        aoi = baker.make(AOI)
        aoi_dict = {
            "area": aoi.area.wkt,
            "name": aoi.name,
            "date": aoi.date,
            "properties": aoi.properties,
        }
        valid_field = aoi_dict[field]
        url = f"{self.endpoint}{aoi.id}/"

        response = api_client().patch(url, {field: valid_field}, format="json")

        assert response.status_code == 200
        assert json.loads(response.content)[field] == valid_field

    def test_delete(self, mocker, api_client):
        aoi = baker.make(AOI)
        url = f"{self.endpoint}{aoi.id}/"

        response = api_client().delete(url)

        assert response.status_code == 204
        assert AOI.objects.all().count() == 0


class TestAOIEndpointsManual:
    """E2E tests manually configured model_bakery."""

    endpoint = "/api/aoi/"

    def test_list(self, api_client, uabb):
        client = api_client()
        uabb(3)
        url = self.endpoint
        response = client.get(url)

        assert response.status_code == 200
        assert len(json.loads(response.content)) == 3

    def test_create(self, api_client, uabb):
        client = api_client()
        aoi = uabb(1)[0]
        valid_data_dict = {
            "area": aoi.area.wkt,
            "name": aoi.name,
            "date": aoi.date,
            "properties": aoi.properties,
        }

        url = self.endpoint

        response = client.post(url, valid_data_dict, format="json")

        assert response.status_code == 201
        assert json.loads(response.content) == valid_data_dict
        assert AOI.objects.last().link

    def test_retrieve(self, api_client, fab):
        aoi = fab()
        aoi = AOI.objects.last()
        expected_json = aoi.__dict__
        expected_json["id"] = aoi.id
        expected_json["area"] = aoi.area.wkt
        expected_json["name"] = aoi.name
        expected_json["date"] = aoi.date.strftime("%Y-%m-%dT%H:%M:%S.%fZ")
        expected_json["properties"] = json(aoi.properties)

        url = f"{self.endpoint}{aoi.id}/"

        response = api_client().get(url)

        assert response.status_code == 200 or response.status_code == 301
        assert json.loads(response.content) == expected_json

    def test_update(self, api_client, uabb):
        old_aoi = uabb(1)[0]
        new_aoi = uabb(1)[0]
        expected_json = new_aoi.__dict__
        expected_json["area"] = new_aoi.area.wkt
        expected_json["name"] = new_aoi.name
        expected_json["date"] = new_aoi.date.strftime("%Y-%m-%dT%H:%M:%S.%fZ")
        expected_json["properties"] = json(new_aoi.properties)

        url = f"{self.endpoint}{old_aoi.id}/"

        response = api_client().put(url, data=expected_json, format="json")

        assert response.status_code == 200 or response.status_code == 301
        assert json.loads(response.content) == expected_json

    @pytest.mark.parametrize(
        "field",
        [
            ("area"),
            ("name"),
            ("date"),
            ("properties"),
        ],
    )
    def test_partial_update(self, api_client, field, uabb):
        uabb(2)
        old_aoi = AOI.objects.first()
        new_aoi = AOI.objects.last()
        valid_field = {
            field: new_aoi.__dict__[field],
        }
        url = f"{self.endpoint}{old_aoi.id}/"

        response = api_client().patch(
            path=url,
            data=valid_field,
            format="json",
        )

        assert response.status_code == 200 or response.status_code == 301
        try:
            assert json.loads(response.content)[field] == valid_field[field]
        except json.decoder.JSONDecodeError:
            pass

    def test_delete(self, api_client, uabb):
        aoi = uabb(1)[0]
        url = f"{self.endpoint}{aoi.id}/"

        response = api_client().delete(url)

        assert response.status_code == 204 or response.status_code == 301
