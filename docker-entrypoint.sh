#!/bin/bash
set -euo pipefail

# Create log directories
mkdir -p ${DEFAULT_LOG_DIR}/aoi
mkdir -p ${DEFAULT_LOG_DIR}/assessment

./manage.py makemigrations
./manage.py migrate
./manage.py collectstatic --noinput
# Create superuser - ignore error if exists
./manage.py createsuperuser --noinput || true

exec "$@"
