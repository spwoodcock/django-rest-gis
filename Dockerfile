ARG REG_URL
ARG PYTHON_VERSION
ARG NODE_VERSION
FROM ${REG_URL}/debian:buster as corp_debian_base



FROM python:${PYTHON_VERSION}-slim-buster as python-deps

ARG http_proxy
ARG https_proxy

COPY --from=corp_debian_base \
    /etc/ssl/certs/ca-certificates.crt \
    /etc/ssl/certs/ca-certificates.crt

WORKDIR /opt/python

RUN pip install pipenv

RUN set -ex \
    && BUILD_DEPS=" \
    build-essential \
    gcc \
    libpcre3-dev \
    libpq-dev \
    git \
    " \
    && apt-get update && apt-get install -y --no-install-recommends $BUILD_DEPS \
    && rm -rf /var/lib/apt/lists/*

COPY "./Pipfile" /opt/python/Pipfile
RUN PIPENV_VENV_IN_PROJECT=1 pipenv install --sequential --deploy
RUN rm /opt/python/Pipfile /opt/python/Pipfile.lock



FROM node:${NODE_VERSION}-buster-slim as node-deps

ARG http_proxy
ARG https_proxy

COPY --from=corp_debian_base \
    /etc/ssl/certs/ca-certificates.crt \
    /etc/ssl/certs/ca-certificates.crt

RUN npm config set proxy $http_proxy \
    && npm config set https-proxy $https_proxy \
    && npm config set cafile /etc/ssl/certs/ca-certificates.crt

WORKDIR /opt/app
COPY ./frontend /opt/app
RUN npm install
RUN npm run build



FROM python:${PYTHON_VERSION}-slim-buster as runtime

ARG http_proxy
ARG https_proxy
ARG MAINTAINER

ENV PYTHONDONTWRITEBYTECODE=1 \
    PYTHONUNBUFFERED=1 \
    PYTHONFAULTHANDLER=1
LABEL "PYTHON_IMG_TAG"="${PYTHON_VERSION}" \
      "MAINTAINER"="${MAINTAINER}" \
      "PYTHONDONTWRITEBYTECODE"="True" \
      "PYTHONUNBUFFERED"="True" \
      "PYTHONFAULTHANDLER"="True"

COPY --from=corp_debian_base \
    /etc/ssl/certs/ca-certificates.crt \
    /etc/ssl/certs/ca-certificates.crt

RUN set -ex \
    && RUN_DEPS=" \
    libpcre3 \
    mime-support \
    postgresql-client \
    libglib2.0-0 \
    libgdal20 \
    libspatialindex-c5 \
    libproj13 \
    " \
    && apt-get update && apt-get install -y --no-install-recommends $RUN_DEPS \
    && rm -rf /var/lib/apt/lists/*

WORKDIR /opt/python
COPY --from=python-deps \
    /opt/python/ \
    /opt/python/
ENV PATH="/opt/python/.venv/bin:$PATH"
# Compile deps to .pyc - faster startup
RUN python -c "import compileall; compileall.compile_path(maxlevels=10, quiet=1)"

# Create a group, user & dir to run app
RUN groupadd -r appuser \
    && useradd --no-log-init -r -g appuser appuser \
    && mkdir -p /opt/app/static \
    && chown -R appuser:appuser /opt/app
WORKDIR /opt/app
COPY --chown=appuser:appuser . .

# Add compiled React frontend
RUN rm -rf /opt/app/frontend
COPY --from=node-deps --chown=appuser:appuser \
    /opt/app /opt/app/frontend

USER appuser:appuser
# Compile code to .pyc
RUN python -m compileall -q /opt/app
# Add any environment variables needed by Django or your settings file here:
ENV DJANGO_SETTINGS_MODULE assessment.settings.prod
# Gunicorn port
EXPOSE 8000
ENTRYPOINT ["/opt/app/docker-entrypoint.sh"]
# Request Line limit (4094) overriden - allows unlimited AJAX HTTP request length
CMD ["gunicorn", "--bind", "0.0.0.0:8000", "--workers", "3", \
    "--worker-tmp-dir", "/dev/shm", "--threads", "6", \
    "--worker-class", "gthread", "--timeout", "120", "--log-file", "-", \
    "--limit-request-line", "0", "assessment.wsgi"]
ARG GIT_COMMIT
LABEL "DJANGO_SETTINGS_MODULE"="assessment.settings.prod" \
      "APP_USER"="appuser" \
      "GIT_COMMIT"="${GIT_COMMIT}"
